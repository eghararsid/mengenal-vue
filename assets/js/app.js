var app = new Vue({
	el: '#app',
	data: {
		button1: '+',
		button2: '-',
		komenArea: '',
		tagal: '29-07-2020',
		nilai: 0,
		bool: [null, null, null],
		komentar: [
			{komen: 'komentar 1', tgl: '29-07-2020', skor: 0},
			{komen: 'komentar 2', tgl: '28-07-2020', skor: 5},
			{komen: 'komentar 2', tgl: '27-07-2020', skor: 5}
		]
	},
	methods: {
		enterPressed: function () {
			this.komentar.push({"komen":this.komenArea,"tgl":this.tagal,"skor":this.nilai});
			this.bool.push(null);
			this.komenArea = '';
		},
		valuePlus: function (index) {
			if (this.bool[index] == null) {
				this.komentar[index]["skor"]++;
				this.bool[index] = true;
			} else if (this.bool[index] == false) {
				this.komentar[index]["skor"] += 2;
				this.bool[index] = true;
			} else if (this.bool[index] == true) {
				this.komentar[index]["skor"] --;
				this.bool[index] = null;
			}
		},
		valueMinus: function (index) {
			if (this.bool[index] == null) {
				this.komentar[index]["skor"]--;
				this.bool[index] = false;
			} else if (this.bool[index] == true) {
				this.komentar[index]["skor"] -= 2;
				this.bool[index] = false;
			} else if (this.bool[index] == false) {
				this.komentar[index]["skor"] ++;
				this.bool[index] = null;
			}
		}
	}
})